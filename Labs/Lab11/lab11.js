export class Lab11 {

	testDefaultParameters(firstParam, secondParam=100){
		return{
			first : firstParam,
			second : secondParam
		};
	}

	testTemplateLiterals(firstName, middleName, lastName){
		return = `${firstName}, ${middleName}, ${lastName}`;
	}

	testMultipleStrings(){
		var multilineString = `This is my multi-line string!
		This is my second line.
		This is my third line.
		This is my fifth line - just kidding, this is the 4th line.`;
			console.log(multilineString);
	}

	testSortWithArrowFunction(unsortedArr){
		return unsortedArr.sort((a, b) => b - a);
	}

}

let lab11 = new Lab11();

let defaultParamResult = lab11.testDefaultParameters('value');
console.log(defaultParamResult.first);
console.log(defaultParamResult.second);

console.log(lab11.testTemplateLiterals('Lily', 'M', 'Noteboom'));

console.log(lab11.testMultipleStrings());

let numbers = [6, 10, 3, 5, 2, 8, 36];
console.log(numbers);
let sorted = lab11.testSortWithArrowFunction(numbers);
console.log(sorted);
